<?php

$M; //Labirinto matmenys
$N;
$LAB; //Labirintas

//Produkcijos
const PRODUCTIONS = [
    ['x' => -1, 'y' => 0],
    ['x' => 0, 'y' => -1],
    ['x' => 1, 'y' => 0],
    ['x' => 0, 'y' => 1]
];

//Ejimo nr.
$L = 2;

//bandymu sk.
$BAND_SK = 0;

//trace l. no.
$TRACE_L_NO = 0;

$PATH_STACK = [];

function EITI(int $x, int $y, bool $dumber, int $d = 0): bool
{
    global $LAB, $BAND_SK, $L, $M, $N, $TRACE_L_NO, $PATH_STACK;

    //jeigu krašte
    if ($x == 0 || $x == $M - 1 || $y == 0 || $y == $N - 1) {
        echo " Terminalinė.\n";
        return true;
    }

    foreach (PRODUCTIONS as $key => $production) {
        $u = $x + $production['x'];
        $v = $y + $production['y'];

        $TRACE_L_NO++;
        $traceD = str_repeat('-', $d);
        $traceLNO = str_pad($TRACE_L_NO, 4, ' ', STR_PAD_LEFT);
        $keyP1 = $key + 1;
        $uP1 = $u + 1;
        $vP1 = $v + 1;

        if ($LAB[$u][$v] == 1) {
            echo "\n$traceLNO) {$traceD}R{$keyP1}. U=$uP1, V=$vP1. Siena.";
            continue;
        } else if ($LAB[$u][$v] != 0) {
            echo "\n$traceLNO) {$traceD}R{$keyP1}. U=$uP1, V=$vP1. Siūlas.";
            continue;
        }

        $PATH_STACK[] = $key;

        $BAND_SK++;
        $L++;
        echo "\n$traceLNO) {$traceD}R{$keyP1}. U=$uP1, V=$vP1. Laisva. L:=L+1=$L. LAB[$uP1,$vP1]:=$L.";

        $LAB[$u][$v] = $L;

        if (EITI($u, $v, $dumber, $d + 1))
            return true;

        $nextL = $L - 1;
        $labValue = $dumber ? 0 : -1;
        echo "\n      ${traceD}-Backtrack iš X=$uP1, Y=$vP1, L=$L. LAB[$uP1,$vP1]:=$labValue. L:=L-1=$nextL.";
        $LAB[$u][$v] = $labValue;
        $L--;

        array_pop($PATH_STACK);
    }

    return false;
}

function initFromFile(string $file): array
{
    global $M, $N, $LAB, $L;

    $L = 2;

    $contents = file_get_contents($file);
    $lines = explode("\n", $contents);
    $startPos = array_pop($lines);

    $N = count($lines);
    $n = 0;

    $LAB = array_fill(0, $N, []);

    foreach ($lines as $line) {
        $cells = array_map(fn(string $cell) => (int) $cell, explode(' ', $line));

        $M = count($cells);
        $m = 0;

        foreach ($cells as $cell) {
            $LAB[$m][$n] = $cell;
            $m++;
        }

        $n++;
    }

    return array_map(fn(string $cell) => ((int) $cell - 1), explode(' ', $startPos));
}

function printTable(): void
{
    global $LAB, $M, $N, $L;

    echo "    Y, V\n";
    echo "    ^\n";

    for ($Y = $N - 1; $Y >= 0; $Y--) {
        $yToPrint = str_pad($Y + 1, 3, ' ', STR_PAD_LEFT);

        echo "$yToPrint | ";

        for ($X = 0; $X < $M; $X++)
            echo str_pad($LAB[$X][$Y], 3, ' ', STR_PAD_LEFT);

        echo "\n";
    }

    $line = str_repeat('-', ($M + 1) * 3 + 1);
    echo "    $line> X, U\n";
    echo "      ";
    for ($X = 0; $X < $M; $X++)
        echo str_pad($X + 1, 3, ' ', STR_PAD_LEFT);
    echo "\n\n";
}

function printMaze(): void
{
    global $L, $startX, $startY;

    echo "PIRMA DALIS. Duomenys\n";
    echo "  1.1. Labirintas\n\n";

    printTable();

    $startXToPrint = $startX + 1;
    $startYToPrint = $startY + 1;
    echo "  1.2 Pradinė padėtis: X=$startXToPrint, Y=$startYToPrint, L=$L.\n\n";

    echo "ANTRA DALIS. Vykdymas\n";
}

function printResults(bool $yra, int $startX, int $startY): void
{
    global $PATH_STACK;

    echo "\nTREČIA DALIS. Rezultatai\n\n";
    if (!$yra) {
        echo "  3.1. Kelias nerastas.\n";
        return;
    }

    echo "  3.1. Kelias rastas.\n\n";
    echo "  3.2. Kelias grafiškai:\n\n";

    printTable();

    $path = join(", ", array_map(fn(int $path) => 'R' . ($path + 1), $PATH_STACK));
    echo "  3.3. Kelias taisyklėmis: $path.\n\n";

    $pathCoordinates = array_reduce(
        $PATH_STACK,
        function (array $carry, int $key) {
            $lastPos = $carry[array_key_last($carry)];
            $carry[] = [
                'x' => $lastPos['x'] + PRODUCTIONS[$key]['x'],
                'y' => $lastPos['y'] + PRODUCTIONS[$key]['y'],
            ];

            return $carry;
        },
        [['x' => $startX, 'y' => $startY]]
    );

    $pathCoordinates = join(', ', array_map(fn(array $coords) => '[X=' . $coords['x'] + 1 . ',Y=' . $coords['y'] + 1 . ']', $pathCoordinates));
    echo "  3.4. Kelias viršūnėmis: $pathCoordinates.\n\n";
}

if ($argc <= 2)
    exit("Naudojimas: php maze.php <failas> <budas>\n* Failas - labirinto failas\n* Budas - V1 arba V2\n");

if (!file_exists($argv[1]))
    exit("Ivesties failas: \"{$argv[1]}\" nebuvo rastas!\n");

if ($argv[2] !== "V1" && $argv[2] !== "V2")
    exit("Būdas gali buti tik arba V1 arba V2!\n");

[$startX, $startY] = initFromFile($argv[1]);

printMaze();

//Nustatom i 2
$LAB[$startX][$startY] = $L;

$yra = EITI($startX, $startY, $argv[2] !== "V1");
printResults($yra, $startX, $startY);