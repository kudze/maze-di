PIRMA DALIS. Duomenys
  1.1. Labirintas

    Y, V
    ^
  7 |   1  1  1  1  1  0  1
  6 |   0  0  0  0  0  0  0
  5 |   1  1  1  1  0  1  1
  4 |   1  0  0  0  0  1  1
  3 |   1  0  1  0  1  1  1
  2 |   1  0  0  0  1  1  1
  1 |   1  1  1  1  1  1  1
    -------------------------> X, U
        1  2  3  4  5  6  7

  1.2 Pradinė padėtis: X=5, Y=4, L=2.

ANTRA DALIS. Vykdymas

   1) R1. U=4, V=4. Laisva. L:=L+1=3. LAB[4,4]:=3.
   2) -R1. U=3, V=4. Laisva. L:=L+1=4. LAB[3,4]:=4.
   3) --R1. U=2, V=4. Laisva. L:=L+1=5. LAB[2,4]:=5.
   4) ---R1. U=1, V=4. Siena.
   5) ---R2. U=2, V=3. Laisva. L:=L+1=6. LAB[2,3]:=6.
   6) ----R1. U=1, V=3. Siena.
   7) ----R2. U=2, V=2. Laisva. L:=L+1=7. LAB[2,2]:=7.
   8) -----R1. U=1, V=2. Siena.
   9) -----R2. U=2, V=1. Siena.
  10) -----R3. U=3, V=2. Laisva. L:=L+1=8. LAB[3,2]:=8.
  11) ------R1. U=2, V=2. Siūlas.
  12) ------R2. U=3, V=1. Siena.
  13) ------R3. U=4, V=2. Laisva. L:=L+1=9. LAB[4,2]:=9.
  14) -------R1. U=3, V=2. Siūlas.
  15) -------R2. U=4, V=1. Siena.
  16) -------R3. U=5, V=2. Siena.
  17) -------R4. U=4, V=3. Laisva. L:=L+1=10. LAB[4,3]:=10.
  18) --------R1. U=3, V=3. Siena.
  19) --------R2. U=4, V=2. Siūlas.
  20) --------R3. U=5, V=3. Siena.
  21) --------R4. U=4, V=4. Siūlas.
      --------Backtrack iš X=4, Y=3, L=10. LAB[4,3]:=0. L:=L-1=9.
      -------Backtrack iš X=4, Y=2, L=9. LAB[4,2]:=0. L:=L-1=8.
  22) ------R4. U=3, V=3. Siena.
      ------Backtrack iš X=3, Y=2, L=8. LAB[3,2]:=0. L:=L-1=7.
  23) -----R4. U=2, V=3. Siūlas.
      -----Backtrack iš X=2, Y=2, L=7. LAB[2,2]:=0. L:=L-1=6.
  24) ----R3. U=3, V=3. Siena.
  25) ----R4. U=2, V=4. Siūlas.
      ----Backtrack iš X=2, Y=3, L=6. LAB[2,3]:=0. L:=L-1=5.
  26) ---R3. U=3, V=4. Siūlas.
  27) ---R4. U=2, V=5. Siena.
      ---Backtrack iš X=2, Y=4, L=5. LAB[2,4]:=0. L:=L-1=4.
  28) --R2. U=3, V=3. Siena.
  29) --R3. U=4, V=4. Siūlas.
  30) --R4. U=3, V=5. Siena.
      --Backtrack iš X=3, Y=4, L=4. LAB[3,4]:=0. L:=L-1=3.
  31) -R2. U=4, V=3. Laisva. L:=L+1=4. LAB[4,3]:=4.
  32) --R1. U=3, V=3. Siena.
  33) --R2. U=4, V=2. Laisva. L:=L+1=5. LAB[4,2]:=5.
  34) ---R1. U=3, V=2. Laisva. L:=L+1=6. LAB[3,2]:=6.
  35) ----R1. U=2, V=2. Laisva. L:=L+1=7. LAB[2,2]:=7.
  36) -----R1. U=1, V=2. Siena.
  37) -----R2. U=2, V=1. Siena.
  38) -----R3. U=3, V=2. Siūlas.
  39) -----R4. U=2, V=3. Laisva. L:=L+1=8. LAB[2,3]:=8.
  40) ------R1. U=1, V=3. Siena.
  41) ------R2. U=2, V=2. Siūlas.
  42) ------R3. U=3, V=3. Siena.
  43) ------R4. U=2, V=4. Laisva. L:=L+1=9. LAB[2,4]:=9.
  44) -------R1. U=1, V=4. Siena.
  45) -------R2. U=2, V=3. Siūlas.
  46) -------R3. U=3, V=4. Laisva. L:=L+1=10. LAB[3,4]:=10.
  47) --------R1. U=2, V=4. Siūlas.
  48) --------R2. U=3, V=3. Siena.
  49) --------R3. U=4, V=4. Siūlas.
  50) --------R4. U=3, V=5. Siena.
      --------Backtrack iš X=3, Y=4, L=10. LAB[3,4]:=0. L:=L-1=9.
  51) -------R4. U=2, V=5. Siena.
      -------Backtrack iš X=2, Y=4, L=9. LAB[2,4]:=0. L:=L-1=8.
      ------Backtrack iš X=2, Y=3, L=8. LAB[2,3]:=0. L:=L-1=7.
      -----Backtrack iš X=2, Y=2, L=7. LAB[2,2]:=0. L:=L-1=6.
  52) ----R2. U=3, V=1. Siena.
  53) ----R3. U=4, V=2. Siūlas.
  54) ----R4. U=3, V=3. Siena.
      ----Backtrack iš X=3, Y=2, L=6. LAB[3,2]:=0. L:=L-1=5.
  55) ---R2. U=4, V=1. Siena.
  56) ---R3. U=5, V=2. Siena.
  57) ---R4. U=4, V=3. Siūlas.
      ---Backtrack iš X=4, Y=2, L=5. LAB[4,2]:=0. L:=L-1=4.
  58) --R3. U=5, V=3. Siena.
  59) --R4. U=4, V=4. Siūlas.
      --Backtrack iš X=4, Y=3, L=4. LAB[4,3]:=0. L:=L-1=3.
  60) -R3. U=5, V=4. Siūlas.
  61) -R4. U=4, V=5. Siena.
      -Backtrack iš X=4, Y=4, L=3. LAB[4,4]:=0. L:=L-1=2.
  62) R2. U=5, V=3. Siena.
  63) R3. U=6, V=4. Siena.
  64) R4. U=5, V=5. Laisva. L:=L+1=3. LAB[5,5]:=3.
  65) -R1. U=4, V=5. Siena.
  66) -R2. U=5, V=4. Siūlas.
  67) -R3. U=6, V=5. Siena.
  68) -R4. U=5, V=6. Laisva. L:=L+1=4. LAB[5,6]:=4.
  69) --R1. U=4, V=6. Laisva. L:=L+1=5. LAB[4,6]:=5.
  70) ---R1. U=3, V=6. Laisva. L:=L+1=6. LAB[3,6]:=6.
  71) ----R1. U=2, V=6. Laisva. L:=L+1=7. LAB[2,6]:=7.
  72) -----R1. U=1, V=6. Laisva. L:=L+1=8. LAB[1,6]:=8. Terminalinė.

TREČIA DALIS. Rezultatai

  3.1. Kelias rastas.

  3.2. Kelias grafiškai:

    Y, V
    ^
  7 |   1  1  1  1  1  0  1
  6 |   8  7  6  5  4  0  0
  5 |   1  1  1  1  3  1  1
  4 |   1  0  0  0  2  1  1
  3 |   1  0  1  0  1  1  1
  2 |   1  0  0  0  1  1  1
  1 |   1  1  1  1  1  1  1
    -------------------------> X, U
        1  2  3  4  5  6  7

  3.3. Kelias taisyklėmis: R4, R4, R1, R1, R1, R1.

  3.4. Kelias viršūnėmis: [X=5,Y=4], [X=5,Y=5], [X=5,Y=6], [X=4,Y=6], [X=3,Y=6], [X=2,Y=6], [X=1,Y=6].

